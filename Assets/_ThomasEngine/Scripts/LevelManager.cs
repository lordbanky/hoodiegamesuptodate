﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour 
{

    [SerializeField]
    private string loadingSceneName = "LoadingScreen";
    [SerializeField]
    private string gameOverSceneName = "GameOverScreen";

    private int curCheckPoint;
    private string curScene;

    private GameDataManager dataManager;

    void Start()
    {
        dataManager = GameManager.instance.GetGameDataManager();
    }

    #region SCENEMANAGEMENT

    public void LoadLevel(string _name)
    {
        SceneManager.LoadScene(_name);
    }

    public void LoadLevel(int _buildInd)
    {
        SceneManager.LoadScene(_buildInd);
    }

    public void LoadLevelWithLoadingScreen(string _levelName)
    {
        dataManager.SetCurLevelName(_levelName);
        SceneManager.LoadScene(loadingSceneName);
    }

    public void ResetCurLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public string GetCurLevelName()
    {
        return SceneManager.GetActiveScene().name;
    }

    public int GetCurLevelInd()
    {
        return SceneManager.GetActiveScene().buildIndex;
    }

    public string GetLevelName(int _buildInd)
    {
        return SceneManager.GetSceneByBuildIndex(_buildInd).name;
    }

    public void LoadNextLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void LoadGameOverScene()
    {
        SceneManager.LoadScene(gameOverSceneName);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    #endregion
}
