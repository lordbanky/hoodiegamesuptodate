﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public static GameManager instance;

    [SerializeField]
    private PlayerUI playerUI;
    [SerializeField]
    private GameDataManager dataManager;
    [SerializeField]
    private SpawnManager spawnManager;
    [SerializeField]
    private LevelManager levelManager;
    [SerializeField]
    private float killHeight = -20;
    [SerializeField]
    private MenuManager menuManager;
    [SerializeField]
    private MusicManager musicManager;

    private bool won;

	// Use this for initialization
	void Awake ()
    {
        instance = this;		
	}

    void Start()
    {
        BeginMusic();
    }
	
    void BeginMusic()
    {
        if (musicManager)
            //play music
            musicManager.PlayBackgroundMusic();
    }

    public void LevelWin(string _nextScene, float _endTime, bool _freezeGame)
    {
        StartCoroutine(StartLevelWin(_nextScene, _endTime, _freezeGame));
    }

    IEnumerator StartLevelWin(string _nextScene, float _endTime, bool _freezeGame)
    {
        won = true;

        //do UI effects

        //freeze game
        float curScale = Time.timeScale;
        if (_freezeGame)
        {
            Time.timeScale = 0;
        }
            
        //play win music
        if (musicManager)
            musicManager.PlayGameOverWinMusic();

        //wait to load next level
        yield return new WaitForSecondsRealtime(_endTime);

        Time.timeScale = curScale;

        //load level
        levelManager.LoadLevelWithLoadingScreen(_nextScene);

    }

    public void GameOverLose(string _nextScene, float _endTime, bool _freezeGame)
    {

    }

    IEnumerator StartGameOver(string _nextScene, float _endTime, bool _freezeGame)
    {
        

        //do UI effects

        //freeze game
        float curScale = Time.timeScale;
        if (_freezeGame)
        {
            Time.timeScale = 0;
        }

        //play win music
        musicManager.PlayGameOverWinMusic();

        //wait to load next level
        yield return new WaitForSecondsRealtime(_endTime);

        Time.timeScale = curScale;

        //load level
        levelManager.LoadLevelWithLoadingScreen(_nextScene);

    }

    public GameDataManager GetGameDataManager()
    {
        return dataManager;
    }

	public PlayerUI GetPlayerUI()
    {
        return playerUI;
    }

    public SpawnManager GetSpawnManager()
    {
        return spawnManager;
    }

    public LevelManager GetLevelManager()
    {
        return levelManager;
    }

    public MenuManager GetMenuManager()
    {
        return menuManager;
    }

    public float GetKillHeight()
    {
        return killHeight;
    }

    public bool IsWon()
    {
        return won;
    }

}
