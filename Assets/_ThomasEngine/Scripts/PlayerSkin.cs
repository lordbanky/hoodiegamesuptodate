﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerSkin : MonoBehaviour
{
    [SerializeField]
    private Sprite skinAvatar;
	
    public Sprite GetSkinAvater()
    {
        return skinAvatar;
    }
}
