﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : MonoBehaviour
{

    [SerializeField]
    private bool freezeOnPause = false;

    private PlayerUI playerUI;

    private bool paused;

    void Start()
    {
        //unfreeze the game
        Time.timeScale = 1;
        //getcomp
        playerUI = GameManager.instance.GetPlayerUI();
    }

    // Update is called once per frame
    void Update()
    {
        GetInputs();
    }

    void GetInputs()
    {
        if (Input.GetButtonDown("Cancel"))
        {
            if (!paused)
            {
                PauseGame(true);
                paused = true;
            }
            else
            {
                PauseGame(false);
                paused = false;
            }
        }
    }

    void PauseGame(bool _pause)
    {
        if (_pause)
        {
            playerUI.PauseMenuSetActive(true);

            if (freezeOnPause)
                Time.timeScale = 0;
        }
            
        else
        {
            playerUI.PauseMenuSetActive(false);

            if (freezeOnPause)
                Time.timeScale = 1;
        }
           
    }

    public bool isPaused()
    {
        return paused;
    }
}
