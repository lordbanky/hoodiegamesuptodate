﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterSelection : MonoBehaviour 
{

    private const string CHARKEY = "CharKey";
    private int curInd;

    [SerializeField]
    private CharacterSelectionUI charUI;
    [SerializeField]
    private Player player;

    private GameDataManager dataManager;

    void Start()
    {
        dataManager = GameManager.instance.GetGameDataManager();

        StartCoroutine(DelaySkinName());
    }

    public void PrevCharacter()
    {
        if (curInd > 0)
            curInd--;
        else
            curInd = player.GetCurSkinCount() - 1;

        SwitchCharacter();
    }

    public void NextCharacter()
    {
        if (curInd < player.GetCurSkinCount() - 1)
            curInd++;
        else
            curInd = 0;

        SwitchCharacter();
    }

    void SwitchCharacter()
    {
        player.SetPlayerSkin(curInd);
        charUI.SetCharName(player.GetCurSkin().name);
    }

    public void ConfirmCharacter()
    {
        dataManager.SetCurPlayerSkin(curInd);
    }

    IEnumerator DelaySkinName()
    {
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();
        charUI.SetCharName(player.GetCurSkin().name);
        curInd = player.GetCurSkinInd();
    }
}
