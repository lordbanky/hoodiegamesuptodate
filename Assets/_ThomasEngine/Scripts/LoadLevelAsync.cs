﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;



public class LoadLevelAsync : MonoBehaviour 
{

    [SerializeField]
    private bool overrideLevelLoad = false;
    [SerializeField]
    private string overrideLevelName;
    [SerializeField]
    private Slider loadingBar;

    [SerializeField]
    private float fadeDelay = 0.3f;
    [SerializeField]
    private float fadeTime = 3;

    [SerializeField]
    private GameObject[] UIObjectsToFade;

    private GameDataManager dataManager;
    private AsyncOperation loadingScene;

    // Use this for initialization
    void Start () 
	{
        dataManager = GameManager.instance.GetGameDataManager();
        StartCoroutine(StartLevelLoading());
	}

    IEnumerator StartLevelLoading()
    {
        while (dataManager.GetCurPlayer() == null)
        {
            yield return new WaitForEndOfFrame();
        }
        BeginLevelLoading();
    }

    void BeginLevelLoading()
    {
        string nextLevel = overrideLevelName;
        if (!overrideLevelLoad)
            nextLevel = dataManager.GetCurSavedLevelName();

        loadingScene = SceneManager.LoadSceneAsync(nextLevel, LoadSceneMode.Single);
        loadingScene.allowSceneActivation = false;

        //UI set
        loadingBar.maxValue = 1;
        loadingBar.minValue = 0;

        StartCoroutine(StartLevelLoad());
    }

    IEnumerator StartLevelLoad()
    {
        while (loadingScene.progress < 0.89f)
        {
            loadingBar.value = loadingScene.progress + 0.1f;
            yield return new WaitForEndOfFrame();
        }
        loadingBar.value = 1;
        loadingScene.allowSceneActivation = true;
        yield return new WaitForSeconds(fadeDelay);
        StartCoroutine(StartFadeIn());
    }

    IEnumerator StartFadeIn()
    {
        float timer = 0;
        float perc = 0;
        while (timer < fadeTime)
        {
            timer += Time.deltaTime;
            if (timer > fadeTime)
                timer = fadeTime;

            perc = timer / fadeTime;
            SetUIAlphas(1 - perc);
            yield return new WaitForEndOfFrame();
        }

        //destroy after fade
        Destroy(gameObject);
    }

    void SetUIAlphas(float _alpha)
    {
        foreach (var obj in UIObjectsToFade)
        {
            
            Text txt = obj.GetComponent<Text>();
            Image img = obj.GetComponent<Image>();
            if (txt)
            {
                Color prevCol = txt.color;
                Color col = new Color(prevCol.r,prevCol.g,prevCol.b,_alpha);
                txt.color = col;
            }
            if (img)
            {
                Color prevCol = img.color;
                Color col = new Color(prevCol.r, prevCol.g, prevCol.b, _alpha);
                img.color = col;
            }
        }
    }
}
