﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour 
{

    [SerializeField]
    private int mainMenuBuildInd = 0;

    private LevelManager lm;

    void Start()
    {
        lm = GameManager.instance.GetLevelManager();
    }

	public void LoadMainMenu()
    {
        lm.LoadLevel(mainMenuBuildInd);
    }

    public void RestartLevel()
    {
        lm.ResetCurLevel();
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
